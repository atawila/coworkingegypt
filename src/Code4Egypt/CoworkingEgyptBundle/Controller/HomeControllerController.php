<?php

namespace Code4Egypt\CoworkingEgyptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeControllerController extends Controller
{
    /**
     *
     */
    public function indexAction()
    {
        return $this->render("CoworkingEgyptBundle:HomeController:index.html.twig", array(

        ));
    }

}
