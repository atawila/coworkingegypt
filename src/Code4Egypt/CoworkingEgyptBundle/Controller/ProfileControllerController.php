<?php

namespace Code4Egypt\CoworkingEgyptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProfileControllerController extends Controller
{
    public function indexAction()
    {
        return $this->render('CoworkingEgyptBundle:ProfileController:index.html.twig', array(
                // ...
            ));
    }

}
