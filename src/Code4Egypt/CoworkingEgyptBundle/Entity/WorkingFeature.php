<?php

namespace Code4Egypt\CoworkingEgyptBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkingFeature
 *
 * @ORM\Table(name="working_feature")
 * @ORM\Entity
 */
class WorkingFeature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Code4Egypt\CoworkingEgyptBundle\Entity\Workspace", mappedBy="workingFeature")
     */
    private $workspace;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->workspace = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
